# Detección automática de grietas y fisuras

## Requerimientos

python3.6 a python3.9

## Instalación

- Crear entorno virtual de python y activarlo
- Instalar las bibliotecas necesarias

```sh
pip install -r requirements.txt
```
