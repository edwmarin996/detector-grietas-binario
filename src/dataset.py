import asyncio
from pathlib import Path

import pandas as pd

from .utils import download_files, indexes_train_validation_test


class Dataset(object):
    """
    """
    CURRENT_DIRECTORY = Path(__file__).parent

    PATH_DATASET_JSON = CURRENT_DIRECTORY / 'fotos_grietas.json'
    PATH_DATASET = CURRENT_DIRECTORY.parent / 'data'
    PATH_IMAGES = PATH_DATASET / 'fotos_grietas'

    def __init__(self):
        self.dataset = self.read_dataset()

    def split_train_validation_test(self, train_size=0.6, validation_size=0.2, test_size=0.2):
        indexes_train, indexes_validation, indexes_test = indexes_train_validation_test(
            len(self.dataset), train_size, validation_size, test_size
        )
        train = self.dataset.iloc[indexes_train]
        validation = self.dataset.iloc[indexes_validation]
        test = self.dataset.iloc[indexes_test]
        return train, validation, test

    @staticmethod
    def get_foto_url(foto):
        BASE_URL = 'https://api.devimed.com.co/media/inventario/grietas_fisuras/'
        return BASE_URL + foto

    def read_dataset(self):
        return pd.read_json(self.PATH_DATASET_JSON)

    def download(self):
        files_to_download = []
        for index, row in self.dataset.iterrows():
            url = self.get_foto_url(row['foto'])
            files_to_download.append((url, row['foto'], self.PATH_IMAGES))

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(download_files(files_to_download))
        loop.close()


if __name__ == '__main__':
    dataset = Dataset()
