import aiohttp
import asyncio
import math
from pathlib import Path
from typing import Iterable, Tuple

import numpy as np
from progress import bar as progress_bar


async def download_file(cliente: aiohttp.ClientSession, url: str, name: str, directory: Path, re_download: bool = False):
    """
    Download a file from a url and save it in a directory.
    """
    file_path = directory / name
    if file_path.exists() and not re_download:
        return

    async with cliente.get(url) as respuesta:
        if respuesta.status == 200:
            archivo_bytes = await respuesta.read()
            if not file_path.parent.exists():
                file_path.parent.mkdir(parents=True)

            with open(file_path, "wb") as archivo:
                archivo.write(archivo_bytes)


async def download_files(files: Iterable[Tuple[str, str, Path]], re_download: bool = False, timeout=3600, limit_per_host=30):
    """
    Download files from a list of tuples (url, name, directory)
    """
    client_timeout = aiohttp.ClientTimeout(total=timeout)
    connector = aiohttp.TCPConnector(limit_per_host=limit_per_host)
    async with aiohttp.ClientSession(connector=connector, timeout=client_timeout) as client:
        tareas = list()
        for file in files:
            tarea = asyncio.ensure_future(download_file(
                client, *file, re_download=re_download))
            tareas.append(tarea)

        bar = progress_bar.IncrementalBar('Processing...', max=len(tareas))
        for cor in asyncio.as_completed(tareas):
            resultado = await cor
            bar.next()
        bar.finish()


def indexes_train_validation_test(length, train_size=0.6, validation_size=0.2, test_size=0.2):
    """
    Return the indexes of the train and test sets.
    """
    assert train_size + validation_size + test_size <= 1, "train_size + validation_size + test_size must be less or equal than 1"
    assert train_size > 0, "train_size must be greater than 0"
    assert validation_size > 0, "validation_size must be greater than 0"
    assert test_size > 0, "test_size must be greater than 0"

    indexes = np.array(range(0, length))
    np.random.shuffle(indexes)

    train_last = math.ceil(train_size*length)
    validation_last = train_last + int(validation_size*length)

    train = indexes[0:train_last]
    validation = indexes[train_last:validation_last]
    test = indexes[validation_last:]

    return train, validation, test
