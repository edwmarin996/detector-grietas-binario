import json
import aiohttp
import asyncio
from pathlib import Path
from progress import bar as progress_bar


PATH_DATASET = Path('../data/')
PATH_PHOTOS = PATH_DATASET / 'fotos_grietas'
PATH_DATASET_FILE = Path('fotos_grietas.json')


def get_foto_url(foto):
    BASE_URL = 'https://api.devimed.com.co/media/inventario/grietas_fisuras/'
    return BASE_URL + foto


def read_dataset():
    with open(PATH_DATASET_FILE, 'r') as archivo:
        return json.loads(archivo.read())


async def download_file(cliente: aiohttp.ClientSession, url: str, name: str, directory: Path, re_download: bool = False):
    file_path = directory / name
    if file_path.exists() and not re_download:
        return

    async with cliente.get(url) as respuesta:
        if respuesta.status == 200:
            archivo_bytes = await respuesta.read()
            if not file_path.parent.exists():
                file_path.parent.mkdir(parents=True)

            with open(file_path, "wb") as archivo:
                archivo.write(archivo_bytes)


async def download_dataset():
    dataset = read_dataset()

    timeout = aiohttp.ClientTimeout(total=60*60)
    connector = aiohttp.TCPConnector(limit_per_host=30)
    async with aiohttp.ClientSession(connector=connector, timeout=timeout) as client:
        tareas = list()
        for element in dataset:
            url = get_foto_url(element['foto'])
            tarea = asyncio.ensure_future(download_file(
                client, url, element['foto'], PATH_PHOTOS))
            tareas.append(tarea)

        bar = progress_bar.IncrementalBar('Processing...', max=len(tareas))
        for cor in asyncio.as_completed(tareas):
            resultado = await cor
            bar.next()
        bar.finish()


def download_photos():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(download_dataset())
    loop.close()


if __name__ == '__main__':
    download_photos()
